LPR over SSH
============

This set of small scripts allows you to control a printer
remotelly using SSH, including printing a local file in
a printer attached to a remote server.
